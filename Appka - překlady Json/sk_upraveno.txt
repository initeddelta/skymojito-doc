﻿{
    "APP_NAME": "SkyMojito",
    "WELCOME_2_TITLE": "Namiešajte si koktail leteniek",
    "WELCOME_2_TEXT": "Skymojito.com Tí prinesie koktail tých najlacnejších leteniek z množstva webov po celej Európe.",
    "WELCOME_3_TEXT": "Spravujte svoje filtre a notifikácia pohodlne na jenom mieste - stačí sa len prihlásiť.",
    "WELCOME_3_TITLE": "",
    "WELCOME_4_TEXT": "Nastav si náš jednoduchý filter a sleduj tie najlepšie akcie do svojej vysnívanej destinácie!",
    "ACTIONS": "Akcia",
    "LOGIN": "Prihlásiť sa",
    "LOGING": "Prihlásenie",
    "LOGIN_FB": "Prihlásiť sa cez Facebook",
    "LOGIN_GOOGLE": "Prihlásiť sa cez Google",
    "LOGIN_NOT": "Alebo pokračuj bez prihlásenia",
    "LOGOUT": "Odhlásiť sa",
    "SETTING_FILTERS": "Nastavení filtrů",
    "SET_FILTERS": "Nastaviť filtre",
    "SET_FLIGHT_SERVERS": "Nastavenie letenkových serverov",
    "FLIGHT_SERVERS": "Letenkové servery",
    "SET_LANGUAGE": "Nastavenie jazykov a meny",
    "OTHER_SETTINGS": "Ďalšie nastavenia",
    "CHOOSE_LANGUAGE": "Vyber si jazyk",
    "SKIP": "Přeskočiť",
    "VERSION": "Verzia",
    "MENU": "Menu",
    "RESERVASION_LINK": "Odkaz na rezerváciu",
    "ACTIONS_MY": "Výber",
    "ACTIONS_ALL": "Všetky akcie",
    "REFRESHER_TEXT": "Načítavam akcie ...",
    "REFRESHER_PULL": "Stiahni pre načítanie dát",
    "FROM": "Odkiaľ",
    "TO": "Kam",
    "RECOMMENDED_FILTER": "Odporúčame",
    "USER_FILTER": "Vyber si vlastný",
    "CHOOSE": "Vybrať",
    "SAVE_SETTINGS": "Uložiť nastavenia",
    "CURRENCY": "Mena",
    "CHOOSE_CURRENCY": "Vyber menu",
    "NOTIFICATIONS": "Notifikácia",
    "NOTIFICATIONS_ALLOW": "Povolit notifikace",
    "NOTIFICATIONS_1X": "Informuj ma 1x denne",
    "NOTIFICATIONS_3X": "Informuj ma 3x denne",
    "NOTIFICATIONS_ALLWAYS": "Informuj ma v real time (ak nemáš nastavenej filtre destinácií, budeme Ti pípať stále :))",
    "LANGUAGE_CURRENCY_TITLE": "Jazyk a mena",
    "SAVED": "Uložené",
    "SETTINGS_SAVED": "Nastavenie bolo uložené",
    "OK": "Ok",
    "GOOGLE_PLUS": "Google Plus",
    "FACEBOOK": "Facebook",
    "TWITTER": "Twitter",
    "LOGGED_IN": "Si prihlásený",
    "LOGGING_IN": "Prebieha prihlasovanie",
    "LOGIN_FAILED": "Prihlásenie sa nepodarilo",
    "ACTION_SHARING_FAILED": "Akciu sa nepodarilo zdieľať.",
    "TWITTER_APP_MISSING": "Akciu sa nepodarilo zdieľať. Skontroluj si, že máš nainštalovanú aplikáciu Twitter.",
    "SAVING_FAILED_TITLE": "Ukladanie zlyhalo",
    "SAVING_FAILED": "Nastavenie sa uložilo lokálne, ale zlyhala synchronizácia so serverom. <br><br>Skontroluj pripojenie k internetu a opakuj akciu neskôr."
}
